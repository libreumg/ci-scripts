#!/bin/bash

if [ ! -d checkstyle-to-json ] ; then
    git clone https://github.com/struckma/checkstyle-to-json.git
#    git clone https://github.com/paweltatarczuk/checkstyle-to-json.git
fi

if ! which python2.7 ; then
    apt-get -y update && apt-get -y install python2.7
fi

python2.7 checkstyle-to-json/checkstyle-to-json.py -G build/reports/checkstyle/main.xml build/reports/checkstyle/main.json
