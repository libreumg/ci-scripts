#!/bin/bash
if [ -d cover2cover ] ; then
#    pushd cover2cover
#    git pull origin
#    popd
    :
else
    git clone https://github.com/rix0rrr/cover2cover.git # MIT license as of 2021-11-03
fi

if ! which xmllint || ! which bc || ! which python2.7 ; then
    apt-get -y update && apt-get -y install libxml2-utils bc python2.7
fi

# JaCoCo XML coverage report to Cobertura XML
python2.7 cover2cover/cover2cover.py build/reports/jacoco/test/jacocoTestReport.xml > cobertura.xml

if which xmllint &&  which bc ; then
    read cv < <(bc -l <<< "100 * `xmllint --xpath 'string(/coverage/@line-rate)' cobertura.xml`" | python2.7 -c "print round(float(raw_input()), 2)")
    echo "Coverage: $cv%" 
fi
