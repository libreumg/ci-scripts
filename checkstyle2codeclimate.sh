#!/bin/bash
if ! which Rscript || ! (Rscript -e 'library("digest")' 2> /dev/null) || 
  ! (Rscript -e 'library("jsonlite")' 2> /dev/null) || ! (Rscript -e 'library("xml2")' 2> /dev/null) ; then
    apt-get -y update && apt-get -y install --no-install-recommends r-cran-xml2 r-cran-jsonlite r-cran-digest
fi

Rscript $SCRIPTS_DIR/checkstyle2codeclimate.R
