#!/bin/bash

if [ -r ~/.nexus_token ] ; then
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
        . ~/.nexus_token
    fi
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
	>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
	exit 1;
    fi
fi

if [ -z "$1" ] ; then
    echo "Deploy file to nexus"
    echo "$0 <file>"
    exit 0;
fi

if  [ -z "$2" ] ; then
    echo "second argument missing, assume to use the repository ship-snapshot-java"
    j="ship-snapshot-java"
else
    j=$2
    echo "using repository $j"
fi

# CI_DEFAULT_BRANCH=main # debug code

if [ -z "$CI_DEFAULT_BRANCH" ] ; then
    >&2 echo "Likely not running from gitlab CI/CD -- variables seem not to have been set"
    exit 1
fi

if [ -n "$(git status --porcelain)" ]; then # https://unix.stackexchange.com/a/155077
    >&2 echo "WARNING: git working tree is not clean (but this might be ok)"
fi

if [ "$CI_COMMIT_REF_NAME"x != "$CI_DEFAULT_BRANCH"x ] ; then
    >&2 echo "not on branch $CI_DEFAULT_BRANCH"
    >&2 echo " -- won't publish from $CI_COMMIT_REF_NAME"
    >&2 echo "Aborting"
    exit 1
fi

v=$(git show HEAD:build.gradle | grep "^version\s*=" | sed -e "s/version\s*=\s*//g" | sed -e "s/'//g")

# https://stackoverflow.com/a/9733456
temp="${v%\'}"
v="${temp#\'}"

mainbranch=$(git rev-parse origin/$CI_DEFAULT_BRANCH 2> /dev/null) || branch=""

if [ -z "$mainbranch" ] ; then
    >&2 echo "no branch named origin/$CI_DEFAULT_BRANCH found"
    exit 1
fi

p=$(git show HEAD:settings.gradle | grep "rootProject.name" | sed -e "s/rootProject.name\s*=\s*//g" | sed -e "s/'//g")

g=$(git show HEAD:build.gradle | grep "^\s*group\s*=" | sed -e "s/group\s*=\s*//g" | sed -e "s/'//g")

if [ -r "$1" ] ; then
    f="$1"
    e=$(echo "$f" | sed -e "s/.*\.//g")
    de=".$e"
    bn=$(basename $f $de)
    echo "try to publish file $bn of type $e as $p on artifact $g in version $v"
    if (curl -s -X 'GET' \
      "$nexus/service/rest/v1/search/assets?name=$p&version=$v&repository=$j" | grep -F $(basename $f)) > /dev/null ; then
      echo $(basename $f) "already exists in $j on $nexus -- won't upload again, force not yet implemented"
      exit 1
    fi
    curl -v -X 'POST' \
      -u "$user:$secret" \
      "$nexus/service/rest/v1/components?repository=$j" \
      -H 'accept: application/json' \
      -H 'Content-Type: multipart/form-data' \
      -F 'maven2.generate-pom=true' \
      -F 'maven2.groupId='"$g" \
      -F 'maven2.artifactId='"$p" \
      -F 'maven2.version='"$v" \
      -F "maven2.asset1=@$1;type=application/x-zip" \
      -F 'maven2.asset1.extension='"$e"
else
    (>&2 echo "Could not find $1")
    exit -1
fi

exit 0;
