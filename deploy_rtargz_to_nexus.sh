#!/bin/bash

if [ -r ~/.nexus_token ] ; then
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
        . ~/.nexus_token
    fi
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
	>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
	exit 1;
    fi
fi

if [ -z "$1" ] ; then
    echo "Deploy r pacakge to nexus"
    echo "$0 <file>"
    exit 0;
fi

if [ -r "$1" ] ; then
    f="$1"
    p=$(basename $f | sed s/.tar.gz$// | cut -d "_" -f 1)
    v=$(basename $f | sed s/.tar.gz$// | cut -d "_" -f 2)
    if (curl -s -X 'GET' \
      "$nexus/service/rest/v1/search/assets?name=$p&version=$v&repository=ship-snapshot-r" | grep -F $(basename $f)) > /dev/null ; then
      echo $(basename $f) "already exists in ship-snapshot-r on $nexus -- won't upload again, force not yet implemented"
      exit 1
    fi
    curl -X 'POST' \
      -u "$user:$secret" \
      "$nexus/service/rest/v1/components?repository=ship-snapshot-r" \
      -H 'accept: application/json' \
      -H 'Content-Type: multipart/form-data' \
      -F "r.asset=@$1;type=application/x-gzip" \
      -F 'r.asset.pathId=src/contrib'
else
    (>&2 echo "Could not find $1")
    exit -1
fi

exit 0;
