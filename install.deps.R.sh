#!/bin/bash

R_VERSION=oldrel

echo -e "\e[31m"
echo -e "-----------------------------------------------------------------\n"
which rig && (rig default $R_VERSION || rig list) || echo "Need rig to select R"
which rig && rig system add-pak
R --version | head -n 1
echo -e "Wanted R $R_VERSION"
echo -e "-----------------------------------------------------------------\n"
echo -e "\e[0m"


apt-get remove -y --purge libxml2 libxml2-dev
apt-get update -y && apt-get install -y libssl-dev=3.2.1-3
apt-get update -y && apt-get install -y libcurl4-openssl-dev libxml2 libxml2-dev pandoc python-is-python3 2to3 pip libmagick++-dev qpdf libpq-dev libnlopt-dev tidy
# texlive-full texlive-xetex -- now, a docker image with pdflatex is used.
pip3 install --break-system-packages anybadge || pip3 install anybadge || true
apt-get install -y libudunits2-dev libmagick++-dev
apt-get install -y libnlopt0
apt-get install -y pandoc
# https://pat-s.me/using-ccache-to-speed-up-r-package-checks-on-travis-ci/
apt-get install -y --no-install-recommends ccache

# https://pat-s.me/using-ccache-to-speed-up-r-package-checks-on-travis-ci/
mkdir $HOME/.R && echo -e 'CXX_STD = CXX14\n\nVER=\nCCACHE=ccache\nCC=$(CCACHE) gcc$(VER) -std=gnu99\nCXX=$(CCACHE) g++$(VER)\nC11=$(CCACHE) g++$(VER)\nC14=$(CCACHE) g++$(VER)\nFC=$(CCACHE) gfortran$(VER)\nF77=$(CCACHE) gfortran$(VER)' > $HOME/.R/Makevars
if [ ! -d $CCACHE_DIR ] ; then
  mkdir $CCACHE_DIR
fi
ln -s $CCACHE_DIR $HOME/.ccache
echo -e 'max_size = 5.0G\nsloppiness = include_file_ctime\nhash_dir=false' > $CCACHE/ccache.conf

rprof=$(Rscript -e 'cat(file.path(R.home(component="etc"), "Rprofile.site"))')
echo ".libPaths( c( \"$R_LIBS_USER\", .libPaths() ) )" >> $rprof
