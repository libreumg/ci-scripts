#!/bin/bash

if [ -r ~/.nexus_token ] ; then
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
        . ~/.nexus_token
    fi
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
	>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
	exit 1;
    fi
fi

if [ -z "$1" ] ; then
    echo "Deploy war file to nexus"
    echo "$0 <file>"
    exit 0;
fi

if [ -r "$1" ] ; then
    f="$1"
    p=$(unzip -p $f META-INF/MANIFEST.MF | awk -F': ' '/Implementation-Title/ {print $2}' | tr -d '\r')
    groupId=$(unzip -p $f META-INF/MANIFEST.MF | awk -F': ' '/Implementation-Vendor-Id/ {print $2}' | tr -d '\r')
    v=$(unzip -p $f META-INF/MANIFEST.MF | awk -F': ' '/Implementation-Version/ {print $2}' | tr -d '\r')
    bn=$(basename $f .war)
    if (curl -s -X 'GET' \
      "$nexus/service/rest/v1/search/assets?name=$p&version=$v&repository=ship-snapshot-java" | grep -F $(basename $f)) > /dev/null ; then
      echo $(basename $f) "already exists in ship-snapshot-r on $nexus -- won't upload again, force not yet implemented"
      exit 1
    fi
    curl -X 'POST' \
      -u "$user:$secret" \
      "$nexus/service/rest/v1/components?repository=ship-snapshot-java" \
      -H 'accept: application/json' \
      -H 'Content-Type: multipart/form-data' \
      -F 'maven2.groupId='"$groupId" \
      -F 'maven2.artifactId='"$p" \
      -F 'maven2.version='"$v" \
      -F "maven2.asset1=@$1;type=application/x-zip" \
      -F 'maven2.asset1.extension=war'
else
    (>&2 echo "Could not find $1")
    exit -1
fi

exit 0;
