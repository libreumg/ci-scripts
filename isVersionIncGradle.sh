#!/bin/bash

# CI_DEFAULT_BRANCH=main # debug code

if [ -z "$CI_DEFAULT_BRANCH" ] ; then
    >&2 echo "Likely not running from gitlab CI/CD -- variables seem not to have been set"
    exit 1
fi

if [ -n "$(git status --porcelain)" ]; then # https://unix.stackexchange.com/a/155077
    >&2 echo "git working tree is not clean"
    >&2 echo " -- should not happen on CI/CD"
    >&2 echo "Aborting"
    exit 1
fi

if [ "$CI_COMMIT_REF_NAME"x != "$CI_DEFAULT_BRANCH"x ] ; then
    >&2 echo "not on branch $CI_DEFAULT_BRANCH"
    >&2 echo " -- won't publish from $CI_COMMIT_REF_NAME"
    >&2 echo "Aborting"
    exit 1
fi

v=$(git show HEAD:build.gradle | awk -F= '$1 ~ " *version *" && $2 ~ "[^ ]" { gsub("^ *", "", $2); gsub(" *$", "", $2); print $2 }')

# https://stackoverflow.com/a/9733456
temp="${v%\'}"
v="${temp#\'}"

branch=$(git rev-parse origin/$v 2> /dev/null) || branch=""

if [ -z "$branch" ] ; then
    >&2 echo "no branch named origin/$v found"
    exit 1
fi

mainbranch=$(git rev-parse origin/$CI_DEFAULT_BRANCH 2> /dev/null) || branch=""

if [ -z "$mainbranch" ] ; then
    >&2 echo "no branch named origin/$CI_DEFAULT_BRANCH found"
    exit 1
fi

if ! git diff --exit-code $branch..$mainbranch -- > /dev/null ; then
    >&2 echo "differences from $v ($branch) detected compared to $CI_DEFAULT_BRANCH ($mainbranch)"
    >&2 echo "  -- version branch $v likely not yet merged into $CI_DEFAULT_BRANCH"
    >&2 echo "Aboring now"
    exit 1
fi

o=$(git show HEAD~1:build.gradle | awk -F= '$1 ~ " *version *" && $2 ~ "[^ ]" { gsub("^ *", "", $2); gsub(" *$", "", $2); print $2 }')
n=$(git show HEAD:build.gradle | awk -F= '$1 ~ " *version *" && $2 ~ "[^ ]" { gsub("^ *", "", $2); gsub(" *$", "", $2); print $2 }')

# TODO: Find a better solution to prevent re-deployments
# if [ x"$o" == x"$n" ] ; then
#     >&2 echo "No version increase on main/master push. Something is wrong. Aborting..."
#     exit 1
# fi

p=$(basename $(pwd))

vendor=$(git show HEAD:build.gradle | awk -F: '/Implementation-Vendor-Id/ { gsub(" ", "", $2); gsub(",$", "", $2); gsub("\"", "", $2); print $2}')

echo -e "\n$p v$v is published: \n\n$v"
echo "$v" > version.txt
echo "$p" > name.txt
echo "$vendor" > vendor.txt
