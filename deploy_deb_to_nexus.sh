#!/bin/bash

if [ -r ~/.nexus_token ] ; then
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
        . ~/.nexus_token
    fi
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
	>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
	exit 1;
    fi
fi

if [ -z "$1" ] ; then
    echo "Deploy debian package to nexus"
    echo "$0 <file> [repository-folder]"
    exit 0;
fi

if [ -z "$2" ] ; then
    echo "missing second argument, using ship-debian-bookworm as repository"
    R="ship-debian-bookworm"
else
    R="$2";
fi

if [ -r "$1" ] ; then
    f="$1"
    p=$(basename $f | sed s/.deb$// | cut -d "_" -f 1)
    v=$(basename $f | sed s/.deb$// | cut -d "_" -f 2)

    apt-get update -y ; apt-get install -y curl

    echo "check for uploading $f in $nexus of package $p in version $v"
    if (curl -s -X 'GET' \
      "$nexus/service/rest/v1/search/assets?name=$p&version=$v&repository=$R" | grep -F $(basename $f)) > /dev/null ; then
      echo $(basename $f) "already exists in $R on $nexus -- won't upload again, force not yet implemented"
    else
      echo "upload $1 to $nexus in repository $R"
      curl -v -X 'POST' \
        -u "$user:$secret" \
        "$nexus/service/rest/v1/components?repository=$R" \
        -H 'accept: application/json' \
        -H 'Content-Type: multipart/form-data' \
        -F "apt.asset=@$1;type=application/vnd.debian.binary-package"
        # or application/x-archive
    fi
else
    (>&2 echo "Could not find $1")
    exit -1
fi

exit 0;
