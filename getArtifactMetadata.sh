#!/bin/bash

# CI_DEFAULT_BRANCH=main # debug code

if [ -z "$CI_DEFAULT_BRANCH" ] ; then
    >&2 echo "Likely not running from gitlab CI/CD -- variables seem not to have been set"
    exit 1
fi

if [ -n "$(git status --porcelain)" ]; then # https://unix.stackexchange.com/a/155077
    >&2 echo "git working tree is not clean"
    >&2 echo " -- should not happen on CI/CD"
    >&2 echo "Aborting"
    exit 1
fi

if [ "$CI_COMMIT_REF_NAME"x != "$CI_DEFAULT_BRANCH"x ] ; then
    >&2 echo "not on branch $CI_DEFAULT_BRANCH"
    >&2 echo " -- won't publish from $CI_COMMIT_REF_NAME"
    >&2 echo "Aborting"
    exit 1
fi

v=$(git show HEAD:build.gradle | grep "^version\s*=" | sed -e "s/version\s*=\s*//g" | sed -e "s/'//g")

# https://stackoverflow.com/a/9733456
temp="${v%\'}"
v="${temp#\'}"

mainbranch=$(git rev-parse origin/$CI_DEFAULT_BRANCH 2> /dev/null) || branch=""

if [ -z "$mainbranch" ] ; then
    >&2 echo "no branch named origin/$CI_DEFAULT_BRANCH found"
    exit 1
fi

# o=$(git show HEAD~1:build.gradle | grep "^version\s*=" | sed -e "s/version\s*=\s*//g" | sed -e "s/'//g")
#n=$(git show HEAD:build.gradle | grep "^version\s*=" | sed -e "s/version\s*=\s*//g" | sed -e "s/'//g")

# TODO: Find a better solution to prevent re-deployments
# if [ x"$o" == x"$n" ] ; then
#     >&2 echo "No version increase on main/master push. Deploying only on version changes. Aborting..."
#     exit 1
# fi

p=$(git show HEAD:settings.gradle | grep "rootProject.name" | sed -e "s/rootProject.name\s*=\s*//g" | sed -e "s/'//g")

vendor=$(git show HEAD:build.gradle | grep "^\s*group\s*=" | sed -e "s/group\s*=\s*//g" | sed -e "s/'//g")

echo -e "\n$p v$v can be published as artifact of $vendor"
echo "$v" > version.txt
echo "$p" > name.txt
echo "$vendor" > vendor.txt
