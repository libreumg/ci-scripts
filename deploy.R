# cat(list.files(path = dirname(getwd()), recursive=TRUE), sep = "\n")
# source(file.path(Sys.getenv("SCRIPTS_DIR"), "install.deps.R"))

version <- read.dcf("DESCRIPTION", fields = "Version")[[1]]

name <- read.dcf("DESCRIPTION", fields = "Package")[[1]]

cat("Deploying", name, " ", version, "...\n")


cat("\n^release.yml$\n", file = ".Rbuildignore", append = TRUE)
cat("\n^package.tar.gz$\n", file = ".Rbuildignore", append = TRUE)

fn <- file.path(dirname(getwd()), sprintf("%s_%s.tar.gz", name, version))

file.copy(to = fn, overwrite = TRUE, recursive = FALSE, from = "package.tar.gz")


if (file.exists(fn)) {
    success <- !system2(file.path(Sys.getenv("SCRIPTS_DIR"), "deploy_rtargz_to_nexus.sh"), args = c(fn))
    if (!success) {
	stop(sprintf("Could not deploy %s", dQuote(fn)))
    }
} else {
    cat("File not found", fn, "\n");
    cat("I have this:\n")
    cat(list.files(path = dirname(getwd()), recursive=!TRUE), sep = "\n")
    quit(status = 1)
}
